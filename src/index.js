import express from 'express'
import config from './conf'
import Api from './api'
import http from 'http'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import cors from 'cors'

let port = config.app['port'];
let app = express();
let whitelist = Object.keys(config.whitelist).map(k => config.whitelist[k]);

app.set("port", port)
app.use(bodyParser.json({
    limit: config.app['bodyLimit']
}))
app.use(cookieParser(config.app['cookie_secret']))

app.use(cors({
    origin: (origin, callback) => {
        console.log(origin)
        let originIsWhitelisted = whitelist.indexOf(origin) !== -1 || typeof origin === "undefined"
        console.log('Is IP allowed: ' + originIsWhitelisted)
        let failureResp = 'You are not authorized to perform this action'
        callback(originIsWhitelisted ? null : failureResp, originIsWhitelisted)
    }
}))


new Api(app).registerGroup()

module.exports = {
    app
}

http
    .createServer(app)
    .on('error', function (ex) {
        console.log(ex);
        console.log('Can\'t connect to server.');
    })
    .listen(port, () => console.log(`Server Started :: ${port}`));