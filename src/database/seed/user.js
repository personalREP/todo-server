const config = require('../../conf')
const mongoose = require("mongoose");
const { hash } = require("bcrypt")
var Schema = mongoose.Schema
const { isEmail } = require('validator')

let users = [
    {
        username: "shayan.ahmed",
        email: "shayanahmed46@yahoo.com",
        password: "test12",
        firstname: "Shayan",
        lastname: "Ahmed"
    },
    {
        username: "john.doe",
        email: "johndoe@gmail.com",
        password: "test12",
        firstname: "John",
        lastname: "Doe"
    },
    {
        username: "michael.waugh",
        email: "michaelwaugh@gmail.com",
        password: "test12",
        firstname: "Michael",
        lastname: "Waugh"
    }
]

function getUserSchema() {
    return new Schema(
        {
            username: {
                type: String,
                validate: {
                    validator: function (v) {
                        return /[A-Za-z0-9.]/.test(v)
                    },
                    message: props => `${props.value} is not a valid username`
                },
                required: [true, 'Username must not contains special characters and spaces.'],
                unique: true
            },
            email: {
                type: String,
                validate: [isEmail]
            },
            password: {
                type: String,
                required: true
            },
            firstname: {
                type: String
            },
            lastname: {
                type: String
            },
            token: {
                type: String
            },
            status: {
                type: String,
                enum: ["active", "inactive", "pending"],
                default: "active"
            },
            isdeleted: {
                type: Boolean,
                default: false
            }
        },
        { timestamps: true }
    )
}

async function seedData(users) {
    if (mongoose.connection.readyState == 0) {
        await mongoose.connect(config.database['mongo_uri'], {
            useNewUrlParser: true,
            dbName: config.database['mongo_dbname']
        });
    }
    if (mongoose) {
        try {
            let model = await mongoose.model('user', getUserSchema())
            await model.deleteMany({ status: 'active' })
            for (let user of users) {
                user.password = await generateHash(user.password)
                await model.create(user)
            }
            console.log("Data Seeded")
            process.exit(0)
        } catch (ex) {
            console.log('Exception')
        }
    }
}

async function generateHash(plainText) {
    return new Promise((resolve, reject) => {
        hash(plainText, 10, function (err, hash) {
            if (!err) {
                resolve(hash)
            } else {
                reject(err)
            }
        });
    }).catch(ex => {
        throw ex
    })
}

console.log("Seeding will first remove all users")
seedData(users)