const config = require('../conf')
const mongoose = require("mongoose");

module.exports.connectToMongoose = async function () {
  // check if already connected to mongo
  if (mongoose.connection.readyState == 0) {
    // connect to host
    await mongoose.connect(config.database['mongo_uri'], {
      useNewUrlParser: true,
      dbName: config.database['mongo_dbname']
    });
    await mongoose.set('useCreateIndex', true);
    return mongoose;
  }
};

module.exports.disConnect = async function () {
  // check if already connected to mongo
  if (mongoose.connection.readyState == 1) {
    await mongoose.disconnect();
  }
};
