const mongoose = require("mongoose")

var Schema = mongoose.Schema

class Task {
  constructor() {
    this.TaskModel = mongoose.model("task", this.getTaskSchema())
  }

  /**
   * Task Schema definition
   */
  getTaskSchema() {
    return new Schema(
      {
        title: {
          type: String
        },
        description: {
          type: String
        },
        created_by: {
          type: Schema.Types.ObjectId,
          ref: "user"
        },
        status: {
          type: String,
          enum: ["completed", "dued", "pending"]
        }
      },
      { timestamps: true }
    )
  }

  async add(data) {
    try {
      let identifier = data._id;
      if (identifier) {
        delete data._id;
        let { _id } = await this.TaskModel.updateOne(
          { _id: identifier },
          data
        );
        return { success: true, data: _id };
      } else {
        let { _id } = await this.TaskModel.create(data);
        return { success: true, data: _id };
      }
    } catch (err) {
      return { success: false, error: err };
    }
  }

  async delete(data) {
    try {
      let identifier = data._id;
      if (identifier) {
        delete data._id;
        let { _id } = await this.TaskModel.deleteOne({
          _id: identifier
        });
        return { success: true, data: _id };
      } else {
        return { success: false, data: "Unable to find data" };
      }
    } catch (err) {
      return { success: false, error: err };
    }
  }

  async get(id, query = {}) {
    try {
      if (id) {
        let type = await this.TaskModel.findOne({
          _id: id
        })
        return { success: true, data: type };
      } else {
        let data = await this.TaskModel.find(query)
        return { success: true, data };
      }
    } catch (ex) {
      return { success: true, error: ex };
    }
  }
}

export default new Task()
