import { comparePassword, generateToken } from "../../utilities";

const mongoose = require("mongoose")
const { isEmail } = require('validator')

var Schema = mongoose.Schema

class User {
  constructor() {
    this.UserModel = mongoose.model("user", this.getUserSchema())
  }

  /**
   * User Schema definition
   */
  getUserSchema() {
    return new Schema(
      {
        username: {
          type: String,
          validate: {
            validator: function (v) {
              return /[A-Za-z0-9.]/.test(v)
            },
            message: props => `${props.value} is not a valid username`
          },
          required: [true, 'Username must not contains special characters and spaces.'],
          unique: true
        },
        email: {
          type: String,
          validate: [isEmail]
        },
        password: {
          type: String,
          required: true
        },
        firstname: {
          type: String
        },
        lastname: {
          type: String
        },
        token: {
          type: String
        },
        status: {
          type: String,
          enum: ["active", "inactive", "pending"],
          default: "active"
        },
        isdeleted: {
          type: Boolean,
          default: false
        }
      },
      { timestamps: true }
    )
  }

  async createUser(data) {
    try {
      let { _id } = await this.UserModel.create(data)
      return { success: true, data: _id }
    } catch (err) {
      return { success: false, error: err }
    }
  }

  async login({ username, password }) {
    try {
      let user = await this.UserModel.findOne({ username })
      if (user) {
        let isCorrectPassword = await comparePassword(password, user.password)
        if (isCorrectPassword) {
          let token = await generateToken({ id: user._id, firstname: user.firstname, lastname: user.lastname, username: user.username, email: user.email, address: user.address, mobile: user.mobile }, '1y')
          await this.UserModel.updateOne({ _id: user._id }, { token, isloggedin: true })
          user.token = token
          delete user.password
          delete user.created_by
          return { success: true, data: user }
        } else {
          return { success: false, error: 'Invalid Password' }
        }
      } else {
        return { success: false, error: 'No User found' }
      }
    } catch (err) {
      return { success: false, error: err }
    }
  }

  async get(id) {
    try {
      if (id) {
        let user = await this.UserModel.findOne({ _id: id, isdeleted: false })
        if (user) {
          return { success: true, data: user }
        } else {
          return { success: false, error: 'User not found' }
        }
      } else {
        let users = await this.UserModel.find({ isdeleted: false })
        if (users) {
          return { success: true, data: users }
        } else {
          return { success: false, error: 'Users not found' }
        }
      }
    } catch (ex) {
      return { success: false, error: ex }
    }
  }
}

export default new User()
