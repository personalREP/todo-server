import { connectToMongoose } from "../connection";

export { default as UserModel } from "./user";
export { default as TaskModel } from "./task";


// Connect to Mongo
connectToMongoose();
