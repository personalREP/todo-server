'use strict';

import {
    Router
} from "express"
import {
    log,
    loggedIn
} from "./middlewares/index"
import { addTask, updateTask, deleteTask, fetchTasks } from "./handlers/task";

export default class TaskAPI {
    constructor() {
        this.router = Router();
        this.registerRoutes();
    }

    registerRoutes() {
        let router = this.router;
        router.post('/', log, loggedIn, addTask)
        router.put('/', log, loggedIn, updateTask)
        router.delete('/', log, loggedIn, deleteTask)
        router.get('/', log, loggedIn, fetchTasks)

    }

    getRouter() {
        return this.router;
    }

    getRouteGroup() {
        return '/task';
    }
}