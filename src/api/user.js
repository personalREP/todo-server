'use strict';

import {
    Router
} from "express"
import {
    log,
    loggedIn
} from "./middlewares/index"
import { addUser, loginUser, getUsers } from "./handlers/user"

export default class UserAPI {
    constructor() {
        this.router = Router();
        this.registerRoutes();
    }

    registerRoutes() {
        let router = this.router;
        router.post('/', log, addUser)
        router.get('/', log, loggedIn, getUsers)
        router.post('/login', log, loginUser)

    }

    getRouter() {
        return this.router;
    }

    getRouteGroup() {
        return '/user';
    }
}