/**
 * Created by ShayanAhmed on 12/7/16.
 */
"use strict"

import input from "../../utilities/input"
import { generateResponse } from "../../utilities"
import { TaskModel } from "../../database/models"

export async function addTask(req, res) {
    let { title, description, status } = input(req);
    let user = input(req, "user");
    let resp = await TaskModel.add({ title, description, status, created_by: user.id });
    if (resp.success) {
        generateResponse(true, "Task created successfuly", resp.data, res);
    } else {
        generateResponse(false, "Unable to add task", null, res);
    }
}

export async function updateTask(req, res) {
    let { _id, title, description, status } = input(req);
    let resp = await TaskModel.add({ _id, title, description, status });
    if (resp.success) {
        generateResponse(true, "Task updated successfuly", resp.data, res);
    } else {
        generateResponse(false, "Unable to update task", null, res);
    }
}

export async function deleteTask(req, res) {
    let { _id } = input(req);
    let resp = await TaskModel.delete({ _id });
    if (resp.success) {
        generateResponse(true, "Task deleted successfuly", resp.data, res);
    } else {
        generateResponse(false, "Unable to delete task", null, res);
    }
}

export async function fetchTasks(req, res) {
    let { id } = input(req, "query");
    let user = input(req, "user");
    let query = {}
    Object.assign(query, { created_by: user.id });
    let resp = await TaskModel.get(id, query);
    if (resp.success) {
        generateResponse(true, "Data fetched successfuly", resp.data, res);
    } else {
        generateResponse(false, "Unable to fetch tasks", null, res);
    }
}