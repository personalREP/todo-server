/**
 * Created by ShayanAhmed on 12/7/16.
 */
"use strict"

import input from "../../utilities/input"
import { generateHash, generateResponse } from "../../utilities"
import { UserModel } from "../../database/models"

export async function addUser(req, res) {
    let {
        email,
        password,
        firstname,
        lastname,
        address,
        mobile,
        status,
        username
    } = input(req)

    let encryptedPassword = await generateHash(password)
    let { id } = input(req, 'user')

    let resp = await UserModel.createUser({
        email,
        password,
        firstname,
        lastname,
        address,
        mobile,
        username,
        password: encryptedPassword,
        status,
        created_by: id
    })

    if (resp.success) {
        generateResponse(true, 'User added successfully', { identifier: resp.data }, res)
    } else {
        generateResponse(false, 'Unable to add user', resp.error.message, res)
    }
}

export async function loginUser(req, res) {
    let { username, password } = input(req)
    let resp = await UserModel.login({ username, password, isadmin: false })
    if (resp.success) {
        generateResponse(true, 'User authenticated successfully', resp.data, res)
    } else {
        generateResponse(false, 'Unable to authenticate user', resp.error.message, res)
    }
}

export async function getUsers(req, res) {
    let resp = await UserModel.get()
    if (resp.success) {
        generateResponse(true, 'Users fetched successfully', resp.data, res)
    } else {
        generateResponse(false, 'Unable to fetch users', resp.error.message, res)
    }
}