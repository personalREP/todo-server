'use strict';

import { verifyToken, generateResponse } from "../../utilities";
import input from "../../utilities/input";

export function log(req, res, next) {
    console.log(req.originalUrl);
    next(); //Go to Next Call
}

export async function loggedIn(req, res, next) {
    let { token } = input(req, 'headers')
    if (token) {
        let decodedToken = await verifyToken(token)
        if (decodedToken) {
            req.user = decodedToken.data
            next()
        } else {
            generateResponse(false, 'Invalid token', null, res)
        }
    } else {
        generateResponse(false, 'Unauthorized request', null, res)
    }
}
