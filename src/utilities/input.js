import { get } from 'lodash'

/**
 * Helper to get input parameters
 * @param {Object} req Request from Azure Functions API
 * @param {string} [key='body'] Which object to pull parameters from
 * @return {Object} Mixed hash of query and body parameters
 */
export default (req, key = 'body') => {
  // client ip
  if (key === 'ip') {
    return get(req, 'headers.x-forwarded-for', '')
      .split(',')
      .shift()
      .split(':')
      .shift()
  }

  // get parameters with fallbacks
  return get(req, key, {})
}
