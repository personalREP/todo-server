import { compare, genSalt, hash } from 'bcrypt'
import { sign, verify } from 'jsonwebtoken'
import error from './error'

export const STATUS_CODE = {
    NOT_FOUND: 404,
    OK: 200,
    BAD_GATEWAY: 502
}

export function parseBody(req) {
    let obj
    if (typeof req.body === 'object') {
        obj = req.body
    } else {
        obj = JSON.parse(req.body)
    }

    return obj
}

export function generateResponse(success, message, data, res) {
    res.status(success === true ? 200 : 400).json({ success, message, data })
}

export async function generateHash(plainText) {
    return new Promise((resolve, reject) => {
        hash(plainText, 10, function (err, hash) {
            if (!err) {
                resolve(hash)
            } else {
                throw buildError({
                    error: err
                }, 'bcrypt-hash')
            }
        });
    }).catch(ex => {
        throw ex
    })
}

export async function comparePassword(plainText, hash) {
    return new Promise((resolve, reject) => {
        compare(plainText, hash, function (err, res) {
            if (!err) {
                resolve(res)
            } else {
                throw buildError({
                    error: err
                }, 'bcrypt-compare')
            }
        });
    }).catch(ex => {
        throw buildError({
            error: ex
        }, 'bcrypt-compare')
    })
}

export async function generateToken(data, expiresIn) {
    return await sign({
        data
    }, 'secret', { expiresIn });
}

export async function verifyToken(token) {
    return new Promise((resolve, reject) => {
        verify(token, 'secret', function (err, decoded) {
            resolve(decoded)
        });
    }).catch(ex => {
        throw ex
    })
}

export function buildError(data, source) {
    // build a new error and return
    return error({
        data,
        source
    })
}

export function pad(d, length) {
    return String(d).padStart(length, '0');
}