export default (data, code = 400) => {
    const error = new Error()
  
    Object.assign(error, {
      data,
      code
    })
  
    return error
  }
  