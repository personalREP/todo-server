# Todo APIs!

REST APIs for ToDo Application

# Configuration

To configure the application `src/conf/config.ini` is the file where you can manage you database credentials, endpoints to which these apis is accessible etc.

# Run

To run the API server you just need to run this command in terminal `npm run start`. This will start you API server on PORT that is defined in configuration file.

# Seeding

You are able to seed the users data. To seed you need to run `npm run seed`. Seeding file is in `src/database/seed/user.js`.

_Note:_ This seeding will remove all the current users then seed the data.

# API Collection

You can also find the postman collection for APIs in the project source.
